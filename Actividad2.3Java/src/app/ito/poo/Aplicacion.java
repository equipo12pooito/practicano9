package app.ito.poo;

import clases.ito.poo.ListaTransportes;
import clases.ito.poo.Transporte;
import clases.ito.poo.Viaje;
import javax.swing.JOptionPane;
import java.time.LocalDate;

public class Aplicacion {
	
	static ListaTransportes lt;
	
	static void menu() {
		inicializa();
		boolean aux = true;
		int respuesta = 0;
		while (aux) {
			String opciones = " Bienvenido a Agencia Transportista S.A. de C.V., elija una opcion: \n1) Agregar un vehiculo."
					+ "\n2) Eliminar un vehiculo." + "\n3) Asignar viaje a un vehiculo. " + "\n4) Cancelar un viaje." 
					+ "\n5) Mostrar lista de vehiculos." + "\n6) Mostrar los viajes asignados a un vehiculo." 
					+ "\n7) Salir.";
			respuesta = Integer.parseInt(JOptionPane.showInputDialog(opciones));
			switch (respuesta) {
			case 1: agregarVehiculo(); break;
			case 2: eliminarVehiculo(); break;
			case 3: asignarViaje(); break;
			case 4: cancelarViaje(); break;
			case 5: mostrarVehiculos(); break;
			case 6: mostrarViajes(); break;
			case 7: aux = false; break;
			default: JOptionPane.showInputDialog(null, "Ingrese una de las opciones mostradas por favor.");
			}
		}
	}

	//====================================
	
	static Transporte capturarUnidad() {
		Transporte t = new Transporte();
		t.setIdentificador(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el ID de la unidad: ")));
		t.setMarca(JOptionPane.showInputDialog("Ingrese la marca de la unidad: "));
		t.setModelo(JOptionPane.showInputDialog("Ingrese el modelo: "));
		t.setCapacidadMaxima(Float.parseFloat(JOptionPane.showInputDialog("Ingrese la capacidad maxima de la unidad:  ")));
		t.setFechaAdquisicion(LocalDate.parse(JOptionPane.showInputDialog("Ingrese la fecha de adquisicion de la unidad en formato (aaaa-mm-dd): ")));
		return t;
	}
	
	static Viaje capturarViaje() {
		Viaje v = new Viaje();
		v.setDescripcionCarga(JOptionPane.showInputDialog("Ingrese la descripcion de la carga: "));
		v.setDestino(JOptionPane.showInputDialog("Ingrese el destino del viaje: "));
		v.setDireccion(JOptionPane.showInputDialog("Ingrese la direccion exacta de destino: "));
		v.setFechaViaje(LocalDate.parse(JOptionPane.showInputDialog("Ingrese la fecha de salida del viaje en formato (aaaa-mm-dd): ")));
		v.setFechaRegreso(LocalDate.parse(JOptionPane.showInputDialog("Ingrese la fecha de regreso del viaje en formato (aaaa-mm-dd): ")));
		v.setMonto(Float.parseFloat(JOptionPane.showInputDialog("Ingrese el monto total del viaje:  ")));
		return v;
	}
	
	//====================================

	static void inicializa() {
		lt = new ListaTransportes();
	}
	
	static void agregarVehiculo() {
		Transporte newTransporte = capturarUnidad();
		if (lt.addItem(newTransporte)) 
			JOptionPane.showMessageDialog(null, "Se ha agredado un nuevo vehiculo.");
		else if (lt.isFull())
			lt.crecerArreglo();
		else if (lt.existeItem(newTransporte))
			JOptionPane.showMessageDialog(null, "ERROR: La unidad ya existe.");
	}
	
	static void eliminarVehiculo() {
		int pos = 0;
		if (lt.isFree())
			JOptionPane.showMessageDialog(null, "No se encontro ninguna unidad registrada en el sistema.");
		else {
			boolean aux = true;
			while (aux) {
				String unidades = "";
				for (int i = 0; i < lt.getSize(); i++)
					unidades = unidades + "\n" + (i + 1) + ") " + (lt.getItem(i)).getIdentificador() + " | " + (lt.getItem(i)).getMarca()
						+ " | " + (lt.getItem(i).getModelo());
				pos = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la unidad que desea dar de baja: " + unidades));
				if ((lt.getSize()) >= pos && pos > 0) {
					lt.clear(lt.getItem(pos - 1));
					JOptionPane.showMessageDialog(null, "La unidad ha sido eliminada exitosamente.");
					aux = false;
				} else
					JOptionPane.showMessageDialog(null, "ERROR: Unidad inexistente.");
			}
		}
	}
	
	static void asignarViaje() {
		int pos = 0;
		if (lt.isFree())
			JOptionPane.showMessageDialog(null, "No se encontro ninguna unidad registrada en el sistema.");
		else {
			boolean aux = true;
			while (aux) {
				String unidades = "";
				for (int i = 0; i < lt.getSize(); i++)
					unidades = unidades + "\n" + (i + 1) + ") " + (lt.getItem(i)).getIdentificador() + " | " + (lt.getItem(i)).getMarca()
						+ " | " + (lt.getItem(i).getModelo());
				pos = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la unidad a la que desea asignar un viaje:  " + unidades));
				if ((lt.getSize()) >= pos && pos > 0) {
					lt.getItem(pos - 1).addViaje(capturarViaje());
					JOptionPane.showMessageDialog(null, "Se ha asigando el viaje exitosamente");
					aux = false;
				} else
					JOptionPane.showMessageDialog(null, "ERROR: Unidad inexistente.");
			}
		}
	}

	static void cancelarViaje() {
		int pos = 0;
		int pos2 = 0;
		if (lt.isFree())
			JOptionPane.showMessageDialog(null, "No se encontro ninguna unidad registrada en el sistema.");
		else {
			boolean aux = true;
			while (aux) {
				String unidades = "";
				String viajes = "";
				for (int i = 0; i < lt.getSize(); i++)
					unidades = unidades + "\n" + (i + 1) + ") " + (lt.getItem(i)).getIdentificador() + " | " + (lt.getItem(i)).getMarca()
							+ " | " + (lt.getItem(i).getModelo()) + " | " + (lt.getItem(i).getViajesRealizados());
				pos = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la unidad a la que desea eleminar un viaje: " + unidades));
				if ((lt.getSize()) >= pos && pos > 0) {
					for (int i = 0; i < lt.getItem(pos - 1).getViajesRealizados().size(); i++)
						viajes = viajes + "\n" + (i + 1) + ") " + (lt.getItem(i)).getViaje(i);
					pos2 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el viaje que desea eliminar " + viajes));
					if ((lt.getItem(pos - 1)).getViajesRealizados().size() >= pos2 && pos2 > 0) {
						if ((lt.getItem(pos - 1)).elimViaje((lt.getItem(pos - 1)).getViaje(pos2 - 1))) {
							JOptionPane.showMessageDialog(null, "El viaje ha sido eliminadao exitosamente.");
							aux = false;
							}
						} else
							JOptionPane.showMessageDialog(null, "ERROR: Viaje inexistente.");
							aux = false;
				} else
					JOptionPane.showMessageDialog(null, "ERROR: Unidad inexistente.");
					aux = false;
			}
		}
	}
	
	static void mostrarVehiculos() {
		if (lt.isFree())
			JOptionPane.showMessageDialog(null, "No se encontro ningun unidad registrada en el sistema.");
		else {
			String unidades = "";
			for (int i = 0; i < lt.getSize(); i++)
				unidades = unidades + "\n" + (i + 1) + ") " + (lt.getItem(i));
			JOptionPane.showMessageDialog(null,"Lista de cuentas registradas en nuestro sistema: "+ unidades);
		}
	}
	
	static void mostrarViajes() {
		if (lt.isFree())
			JOptionPane.showMessageDialog(null, "No se encontro ningun unidad registrada en el sistema.");
		else {
			String unidades = "";
			for (int i = 0; i < lt.getSize(); i++)
				unidades = unidades + "\n" + (i + 1) + ") " + (lt.getItem(i)).getIdentificador() + " | " + (lt.getItem(i)).getMarca()
						+ " | " + (lt.getItem(i).getModelo()) + " | " + (lt.getItem(i).getViajesRealizados());
			JOptionPane.showMessageDialog(null,"Lista de unidades y sus viajes realizados en nuestro sistema: "+ unidades);
		}
	}
}

