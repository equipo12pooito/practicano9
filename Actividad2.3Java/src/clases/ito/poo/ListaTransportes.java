package clases.ito.poo;

public class ListaTransportes implements Arreglo <Transporte> {
	
	private Transporte unidades[] = null;
	private int ultimo = 0;
	private final int I = 5;

	public ListaTransportes() {
		super();
		this.unidades = new Transporte[I];
		this.ultimo = -1;
	}
	
	public void crecerArreglo() {
		Transporte copia[] = new Transporte[this.unidades.length + I];
		for (int i=0; i < unidades.length; i++)
			copia[i] = this.unidades[i];
		unidades = copia;
	}
	
	//====================================

	@Override
	public boolean addItem(Transporte item) {
		boolean add = false;
		if (!this.existeItem(item)) {
			if (this.isFull()) 
				crecerArreglo();
			int j = 0;
			for(; j <= this.ultimo; j++)
				if (item.compareTo(this.unidades[j]) < 0) 
					break;		
			for(int i = this.ultimo; i >= j; i--)
				unidades[i + 1] = unidades[i];
			this.unidades[j] = item;
			this.ultimo++;
			add = true;
		}
	return add;
	}


	@Override
	public boolean existeItem(Transporte item) {
		boolean existe = false;
		for (int i = 0; i <= this.ultimo; i++)
			if (item.equals(this.unidades[i]) == true) {
				existe = true; 
				break;
			}		
		return existe;
	}

	@Override
	public Transporte getItem(int pos) {
		Transporte t = null;
		if (pos <= this.ultimo && !this.isFree())
			t = this.unidades[pos];
		return t;
	}

	@Override
	public int getSize() {
		return this.ultimo+1;
	}

	@Override
	public boolean clear(Transporte item) {
		boolean borrar = false;
		if (this.existeItem(item)) {
			int i = 0;
			for (; i <= this.ultimo; i++)
				if (item.equals(this.unidades[i]) == true)
					break;
			for(;i <= this.ultimo; i++)
				unidades[i] = unidades[i + 1];
			this.ultimo--;
			borrar = true;
		}
		return borrar;
	}

	@Override
	public boolean isFree() {
		return this.ultimo==-1;
	}

	@Override
	public boolean isFull() {
		return this.ultimo+1==this.unidades.length;
	}

}
