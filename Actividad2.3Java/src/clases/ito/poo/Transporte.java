package clases.ito.poo;

import java.time.LocalDate;
import java.util.ArrayList;

public class Transporte implements Comparable <Transporte>{
	
	private int identificador;
	private String marca = "";
	private String modelo = "";
	private float capacidadMaxima = 0F;
	private LocalDate fechaAdquisicion = null;
	private ArrayList<Viaje> viajesRealizados = new ArrayList<Viaje>();

	public Transporte() {
		super();
	}

	public Transporte(int identificador, String marca, String modelo, float capacidadMaxima, LocalDate fechaAdquisicion) {
		super();
		this.identificador = identificador;
		this.marca = marca;
		this.modelo = modelo;
		this.capacidadMaxima = capacidadMaxima;
		this.fechaAdquisicion = fechaAdquisicion;
	}

	//========================================
	
	public int getIdentificador() {
		return this.identificador;
	}
	
	public void setIdentificador(int identificador) {
		this.identificador = identificador;
	}

	public String getMarca() {
		return this.marca;
	}

	public void setMarca(String newMarca) {
		this.marca = newMarca;
	}

	public String getModelo() {
		return this.modelo;
	}

	public void setModelo(String newModelo) {
		this.modelo = newModelo;
	}

	public float getCapacidadMaxima() {
		return this.capacidadMaxima;
	}

	public void setCapacidadMaxima(float newCapacidadMaxima) {
		this.capacidadMaxima = newCapacidadMaxima;
	}

	public LocalDate getFechaAdquisicion() {
		return this.fechaAdquisicion;
	}

	public void setFechaAdquisicion(LocalDate newFechaAdquisicion) {
		this.fechaAdquisicion = newFechaAdquisicion;
	}

	
	public ArrayList<Viaje> getViajesRealizados() {
		return this.viajesRealizados;
	}
	
	public Viaje getViaje(int i) {
		Viaje o;
		if (i > viajesRealizados.size() || i < 0)
			o = new Viaje();
		else o = this.getViajesRealizados().get(i);
		return o;
	}

	//========================================
	

	public boolean addViaje(Viaje newViaje) {
		boolean addViaje = false;
		addViaje = this.viajesRealizados.add(newViaje);
		return addViaje;
	}

	public boolean elimViaje(Viaje viaje) {
		boolean elimViaje = false;
		elimViaje = this.viajesRealizados.remove(viaje);
		return elimViaje;
	}
	
	
	//========================================
	
	@Override
	public String toString() {
		return "ID: " + identificador+ ", Transporte " + marca + " " + modelo + " \nCapacidad Maxima: " + capacidadMaxima + " TON"
				+ "\nFecha Adquisicion: " + fechaAdquisicion;
	}

	@Override
	public int compareTo(Transporte o) {
		if (identificador < o.getIdentificador())
			return -1;
		else if (identificador > o.getIdentificador())
			return 1;
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		Transporte o = (Transporte) obj;
		if (identificador == o.getIdentificador())
			if (marca == o.getMarca())
				if (modelo == o.getModelo())
					if (fechaAdquisicion.equals(o.getFechaAdquisicion()))
						return true;
		return false;
	}
	
}
